module Main exposing (main)

import Browser
import Element
import Element.Input
import Helpers
import Html
import Ports
import Task
import Time
import TimeHelpers
    exposing
        ( advancePhase
        , toHumanTime
        )
import Types
    exposing
        ( AppState(..)
        , CurrentState
        , Model
        , Msg(..)
        , Phase(..)
        )


main : Program {} Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


init : {} -> ( Model, Cmd Msg )
init _ =
    -- TODO pass time into here
    -- Model is initially populated with default, certainly incorrect values
    -- Until we know the correct ones
    ( { curState =
            { curTime = Time.millisToPosix 0
            , zone = Time.utc
            , state = Init
            , currentTaskDesc = Nothing
            }
      , msgHist = []
      }
    , Cmd.batch
        [ Task.perform NewTime Time.now
        , Task.perform Zone Time.here
        ]
    )


view : Model -> Html.Html Msg
view model =
    Element.layout [] <|
        Element.column
            []
            [ Element.text ("The time is " ++ toHumanTime model.curState.curTime model.curState.zone)
            , case model.curState.state of
                Init ->
                    viewStartWorkday model.curState

                WorkdayStarted phase maybeOverridePhaseTime ->
                    viewWorkday model.curState phase
            , viewHistory model
            ]



-- TODO rename these functions according to current AppState, consider breaking out into nested TEA


viewStartWorkday : CurrentState -> Element.Element Msg
viewStartWorkday curState =
    Element.column
        []
        [ Element.text "Hello! Let's get to work on:"
        , viewInputTaskDesc curState
        , Element.Input.button []
            { onPress = Just StartWorkday
            , label = Element.text "Click me to start workday"
            }
        ]


viewWorkday : CurrentState -> Phase -> Element.Element Msg
viewWorkday curState phase =
    case phase of
        WorkTime ->
            viewWorkTime curState

        BreakTime ->
            viewBreakTime curState


viewWorkTime : CurrentState -> Element.Element Msg
viewWorkTime curState =
    Element.column
        []
        [ viewCurrentWorkTask curState
        , viewNextBreakStartRelative curState
        ]


viewCurrentWorkTask : CurrentState -> Element.Element Msg
viewCurrentWorkTask curState =
    case curState.currentTaskDesc of
        Just taskDesc ->
            Element.text ("You're working on: " ++ taskDesc)

        Nothing ->
            Element.text "You're working"


viewNextBreakStartRelative : CurrentState -> Element.Element Msg
viewNextBreakStartRelative curState =
    Element.text "Break time in TODO X minutes"


viewBreakTime : CurrentState -> Element.Element Msg
viewBreakTime curState =
    Element.column
        []
        [ Element.text "It's break time"
        , viewNextWorkStartRelative curState
        ]


viewNextWorkStartRelative : CurrentState -> Element.Element Msg
viewNextWorkStartRelative curState =
    Element.text "Back to work in TODO X minutes"


viewInputTaskDesc : CurrentState -> Element.Element Msg
viewInputTaskDesc curState =
    Element.Input.text []
        { onChange = InputTaskDesc
        , text = curState.currentTaskDesc |> Maybe.withDefault ""
        , placeholder = Nothing
        , label = Element.Input.labelHidden "What are you working on?"
        }


viewHistory : Model -> Element.Element Msg
viewHistory model =
    let
        viewHistItem : ( Time.Posix, Msg ) -> Element.Element Msg
        viewHistItem ( time, msg ) =
            toHumanTime time model.curState.zone
                ++ " -- "
                ++ Debug.toString msg
                |> Element.text

        items =
            model.msgHist
                |> List.map viewHistItem
    in
    Element.column
        []
    <|
        List.concat
            [ [ Element.text "History:" ]
            , items
            ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        ( newModel, cmd ) =
            update_ msg model

        newMsgHist =
            case msg of
                NewTime _ ->
                    -- Don't store NewTime Msgs
                    newModel.msgHist

                _ ->
                    ( newModel.curState.curTime, msg ) :: newModel.msgHist
    in
    ( { newModel | msgHist = newMsgHist }, cmd )


update_ : Msg -> Model -> ( Model, Cmd Msg )
update_ msg model =
    let
        prevCurState =
            model.curState
    in
    case msg of
        NewTime newTime ->
            let
                newCurState =
                    { prevCurState
                        | curTime = newTime
                    }

                newModel =
                    { model | curState = newCurState }
            in
            case prevCurState.state of
                Init ->
                    ( { model | curState = newCurState }, Cmd.none )

                WorkdayStarted prevPhase maybeOverridePhaseTime ->
                    case advancePhase newModel of
                        Nothing ->
                            ( { model | curState = newCurState }, Cmd.none )

                        Just ( newPhase, maybeNewOverridePhaseTime ) ->
                            update (NewPhase newPhase maybeNewOverridePhaseTime) { model | curState = newCurState }

        Zone zone ->
            ( { model | curState = { prevCurState | zone = zone } }, Cmd.none )

        InputTaskDesc taskDesc ->
            ( { model | curState = { prevCurState | currentTaskDesc = Just taskDesc } }, Cmd.none )

        StartWorkday ->
            let
                phase =
                    -- TODO if starting workday during a break, override this phase to go  until the next break
                    -- TODO need a helper function nextBreakTimeStart, take difference between that and current time
                    WorkTime
            in
            ( { model | curState = { prevCurState | state = WorkdayStarted phase Nothing } }, Cmd.none )

        NewPhase phase maybeNewOverridePhaseTime ->
            let
                soundFileName =
                    phase
                        |> Helpers.soundForPhase
                        |> Helpers.soundFileName

                newAppState =
                    WorkdayStarted phase maybeNewOverridePhaseTime

                newCurState =
                    { prevCurState | state = newAppState }
            in
            ( { model | curState = newCurState }
            , Cmd.batch
                [ Ports.playSound soundFileName
                , Ports.desktopNotification (Debug.toString phase)
                ]
            )


subscriptions : Model -> Sub Msg
subscriptions _ =
    -- Update twice a second
    Time.every (1 * 500) NewTime
