port module Ports exposing (desktopNotification, playSound)


port playSound : String -> Cmd msg


port desktopNotification : String -> Cmd msg
