module Types exposing (AppState(..), CurrentState, Model, Msg(..), OverridePhaseTime, Phase(..), Sound(..))

import Dict
import Time


type alias Model =
    { curState : CurrentState
    , msgHist : List ( Time.Posix, Msg )
    }


type alias CurrentState =
    { curTime : Time.Posix
    , zone : Time.Zone
    , state : AppState
    , currentTaskDesc : Maybe TaskDesc
    }


type AppState
    = Init
    | WorkdayStarted Phase (Maybe OverridePhaseTime)


type alias OverridePhaseTime =
    { startTime : Time.Posix
    , duration : PhaseDurationMillis
    }


type Msg
    = NewTime Time.Posix
    | Zone Time.Zone
    | InputTaskDesc TaskDesc
    | StartWorkday
    | NewPhase Phase (Maybe OverridePhaseTime)


type alias TaskDesc =
    String


type Phase
    = WorkTime
    | BreakTime


type alias PhaseDurationMillis =
    Int


type Sound
    = TimerBeep
    | DJAirHorn
