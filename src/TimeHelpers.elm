module TimeHelpers exposing (advancePhase, toHumanTime)

import Time
    exposing
        ( Posix
        , Zone
        , toHour
        , toMinute
        , toSecond
        )
import Types exposing (AppState(..), Model, OverridePhaseTime, Phase(..))


toHumanTime : Posix -> Zone -> String
toHumanTime time zone =
    let
        timeParts =
            [ toHour, toMinute, toSecond ]

        toHumanTimePart : (Zone -> Posix -> Int) -> String
        toHumanTimePart timePartFunc =
            timePartFunc zone time
                |> String.fromInt
                |> String.padLeft 2 '0'
    in
    timeParts
        |> List.map toHumanTimePart
        |> String.join ":"


advancePhase : Model -> Maybe ( Phase, Maybe OverridePhaseTime )
advancePhase model =
    -- TODO move this to another module, maybe StateHelpers.elm
    case model.curState.state of
        WorkdayStarted curPhase maybeOverridePhaseTime ->
            case maybeOverridePhaseTime of
                Nothing ->
                    let
                        minute =
                            toMinute model.curState.zone model.curState.curTime

                        newPhase =
                            if minute < 5 then
                                BreakTime

                            else if minute < 30 then
                                WorkTime

                            else if minute < 35 then
                                BreakTime

                            else
                                WorkTime
                    in
                    if newPhase /= curPhase then
                        Just ( newPhase, Nothing )

                    else
                        Nothing

                Just _ ->
                    -- TODO implement this
                    Nothing

        _ ->
            Nothing
