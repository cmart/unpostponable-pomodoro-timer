Pomodoro timer that cannot be paused or postponed. Helps the user avoid procrastinating. 25-minute work sessions begin at 5 minutes past the hour, and 35 minutes past.

## To Compile

```
elm make src/Main.elm --output elm.js
```

Then open index.html

## To compile and run with live-reload on code changes

```
npx elm-live src/Main.elm -- --output elm.js
```

## Todo
- Make noises when entering work and break times
- Pass initial time and zone in via flags
- Buttons to preview chime noise
- Browser notifications to desktop?
